@extends('layouts.app')

@section('content')
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <p class="text-center text-muted">Synax Installer</p>
                            <h4 class="text-center">Install your Software</h4>
                            <form action="{{route('installer')}}" method="POST" class="installer row"> 
                                @csrf
                                <div class="form-group col-md-6">
                                    <label for="name">Name of Your System</label>
                                    <input type="text" id="name" class="form-control" name="name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="url">System Installation URL</label>
                                    <input type="text" id="url" class="form-control" name="url" value="{{stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://'}}{{$_SERVER['SERVER_NAME']}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <br>
                                    <p class="text-black-50">MySQL Database Details</p>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="hostname">Hostname</label>
                                    <input type="text" id="hostname" class="form-control" value="localhost" name="hostname">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="database">MySQL DB Name</label>
                                    <input type="text" id="database" class="form-control" name="database">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="username">MySQL Username</label>
                                    <input type="text" id="username" class="form-control" name="username">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="db_password">MySQL Password</label>
                                    <input type="text" id="db_password" class="form-control" name="db_password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Install</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
